-- Your SQL goes here

CREATE TABLE user
(
    id               INT AUTO_INCREMENT,
    discord_id       BIGINT UNSIGNED NOT NULL,
    discord_username VARCHAR(50)     NOT NULL,
    swgoh_ally_code  BIGINT UNSIGNED NOT NULL,
    swgoh_username   VARCHAR(50)     NOT NULL,
    CONSTRAINT user_pk
        PRIMARY KEY (id)
);

CREATE UNIQUE INDEX user_discord_id_uindex
    ON user (discord_id);

CREATE UNIQUE INDEX user_discord_username_uindex
    ON user (discord_username);

CREATE UNIQUE INDEX user_swgoh_ally_code_uindex
    ON user (swgoh_ally_code);

CREATE UNIQUE INDEX user_swgoh_username_uindex
    ON user (swgoh_username);
