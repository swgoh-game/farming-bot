-- This file should undo anything in `up.sql`

ALTER TABLE user_farm
    DROP COLUMN stars_finished,
    DROP COLUMN gears_finished;
