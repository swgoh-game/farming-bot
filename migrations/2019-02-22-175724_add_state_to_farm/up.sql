-- Your SQL goes here

ALTER TABLE user_farm
    ADD stars_finished BOOL NOT NULL DEFAULT FALSE AFTER stars_goal,
    ADD gears_finished BOOL NOT NULL DEFAULT FALSE AFTER gears_goal;
