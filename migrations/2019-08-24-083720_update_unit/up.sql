-- Your SQL goes here

ALTER TABLE user_farm
    DROP FOREIGN KEY user_farm_unit_id_fk;

ALTER TABLE unit
    MODIFY id INT AUTO_INCREMENT;

ALTER TABLE user_farm
    ADD CONSTRAINT user_farm_unit_id_fk
        FOREIGN KEY (unit_id) REFERENCES unit (id);
