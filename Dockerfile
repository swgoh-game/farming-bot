# Build image
# To work, this image need DISCORD_TOKEN and DATABASE_URL env var.
FROM rust:latest as build

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #
#                                                 ARGUMENTS                                                            #
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #

ARG VERSION
ARG RELEASE_DATE

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #
#                                                 LABELS                                                               #
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #

LABEL vendor="dawen"
LABEL ch.dawen.swgoh_farming_bot.version=${VERSION}
LABEL ch.dawen.swgoh_farming_bot.release-date=${RELEASE_DATE}

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #
#                                                 INSTRUCTIONS                                                         #
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ #

ENV APP_ENV=prod
ENV RUST_LOG="error,swgoh_farming_bot=info"

WORKDIR /tmp/code/

COPY src/ /tmp/code/src/
COPY migrations/ /tmp/code/migrations/
COPY Cargo.toml /tmp/code/
COPY Cargo.lock /tmp/code/
COPY diesel.toml /tmp/code/

RUN cargo install --path .
RUN cargo clean

ENTRYPOINT swgoh_farming_bot
