# SWGoH farming bot

[![dependency status](https://deps.rs/repo/gitlab/swgoh-game/farming-bot/status.svg)](https://deps.rs/repo/gitlab/swgoh-game/farming-bot) 
[![pipeline status](https://gitlab.com/swgoh-game/farming-bot/badges/master/pipeline.svg)](https://gitlab.com/swgoh-game/farming-bot/commits/master)

A Discord bot to manage and track farming in SWGoH

## Requirements
This bot works only with MySQL.

`DISCORD_TOKEN` and `DATABASE_URL` env var must be set to work.

`DATABASE_URL` must be a valid MySQL format like `mysql://[[user]:[password]@]host[:port][/database]`.

## Usage

### With Docker (recommended)
All release images are in this registry `registry.gitlab.com/swgoh-game/farming-bot/swgoh_farming_bot`.
See [container registry](https://gitlab.com/swgoh-game/farming-bot/container_registry) to see all images.

1. Pull latest docker image `docker pull registry.gitlab.com/swgoh-game/farming-bot/swgoh_farming_bot:latest`.
2. Run image `docker run --env DISCORD_TOKEN=yourtoken --env DATABASE_URL=yourdburl registry.gitlab.com/swgoh-game/farming-bot/swgoh_farming_bot`.

### With Cargo
Run cargo install `swgoh_farming_bot`. The database migrations are run at application startup.
To run use `env APP_ENV=prod DISCORD_TOKEN=yourdiscordtoken DATABASE_URL=yourdburl swgoh_farming_bot`.

To specify log level, add `RUST_LOG` env var.

### From sources

#### Requirements
Diesel must be installed with Mysql feature. See [diesel documentation](https://diesel.rs/guides/getting-started/) to install it

##### Compilation requirements
* C compiler for example GCC
* SSL library
* pkg-config (linux only)
* MySQL Client library

#### Installation
Follow steps bellow to install and run this bot.
1. Clone repository
2. Copy [`.env.dist`](.env.dist) file and rename it to `.env`. Change content to the right values. Or use environment variables.
3. Start bot with `cargo run --release` or build with `cargo build --release` and run  with `./target/release/swgoh-farming-bot`.

### After startup
Inside discord run the `!unit update` command. Only the bot owner can run this command.

## Versioning
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html) and use [git flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) branching model.

## Roadmap
- [x] `unit update` command who get all existing units (character and ships) from [swgoh.gg](https://swgoh.gg/api/).
- [x] `unit farmed` command who get all members that farm this unit.
- [ ] `farm gears` command to register whit units is actually gears up.
- [x] `user ignore` command to ignore a user during given days or definitively.
- [ ] Add command for customisation of bot (officer roles, announce interval).
- [ ] Cron to regularly update information (eg: gear of user units).
- [x] Allow installation with cargo.

All new ideas are welcome.

## Copyright and Licence
Copyright 2019 David Wittwer and Contributors

This project is licensed under GNU GPLv3 license. See [license file](LICENSE)
