use crate::models::user::User;
use diesel::{mysql::MysqlConnection, prelude::*, r2d2::ConnectionManager};
use r2d2::{Pool, PooledConnection};
use serenity::{
    futures::StreamExt,
    http::Http,
    model::guild::MembersIter,
    model::{
        prelude::{GuildId, RoleId},
        user::User as DiscordUser,
    },
    prelude::Context,
};
use std::{env, fmt};

lazy_static! {
    pub static ref POOL: Pool<ConnectionManager<MysqlConnection>> = init_pool();
}

pub trait LogError {
    fn log_err(self, msg: Option<&str>) -> Self;
}

impl<T, E> LogError for Result<T, E>
where
    E: fmt::Display,
{
    fn log_err(self, msg: Option<&str>) -> Self {
        if let Err(e) = &self {
            match msg {
                Some(m) => error!("{}: {}", m, e),
                None => error!("{}", e),
            }
        }
        self
    }
}

#[derive(fmt::Debug)]
pub struct CmdError {
    pub message: String,
}

impl fmt::Display for CmdError {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "{}", self.message)
    }
}

impl std::error::Error for CmdError {}

impl From<serenity::framework::standard::ArgError<std::string::ParseError>> for CmdError {
    fn from(e: serenity::framework::standard::ArgError<std::string::ParseError>) -> Self {
        CmdError {
            message: e.to_string(),
        }
    }
}

impl From<serenity::framework::standard::ArgError<std::num::ParseIntError>> for CmdError {
    fn from(e: serenity::framework::standard::ArgError<std::num::ParseIntError>) -> Self {
        CmdError {
            message: e.to_string(),
        }
    }
}

impl From<diesel::result::Error> for CmdError {
    fn from(e: diesel::result::Error) -> Self {
        CmdError {
            message: e.to_string(),
        }
    }
}

impl From<std::num::ParseIntError> for CmdError {
    fn from(e: std::num::ParseIntError) -> Self {
        CmdError {
            message: e.to_string(),
        }
    }
}

impl From<serenity::Error> for CmdError {
    fn from(e: serenity::Error) -> Self {
        CmdError {
            message: e.to_string(),
        }
    }
}

impl From<chrono::ParseError> for CmdError {
    fn from(e: chrono::ParseError) -> Self {
        CmdError {
            message: format!("{}", e),
        }
    }
}

fn init_pool() -> Pool<ConnectionManager<MysqlConnection>> {
    let manager = match env::var("DATABASE_URL") {
        Ok(db_url) => ConnectionManager::<MysqlConnection>::new(&db_url),
        Err(_) => {
            panic!("DATABASE_URL environment variable must be set")
        }
    };

    Pool::builder()
        .build(manager)
        .expect("Failed to create pool.")
}

pub fn get_connection() -> PooledConnection<ConnectionManager<MysqlConnection>> {
    POOL.get().unwrap()
}

pub fn get_current_user(asked_discord_id: u64) -> Result<User, CmdError> {
    use crate::schema::user::dsl::*;

    let conn = get_connection();

    let users: Vec<User> = user
        .filter(discord_id.eq(asked_discord_id))
        .load::<User>(&conn)?;

    if users.is_empty() {
        Err(CmdError {
            message: "Unknown user! please use register before.".to_string(),
        })
    } else {
        Ok(users[0].clone())
    }
}

pub async fn get_user_for_roles(
    ctx: &Context,
    roles: &[RoleId],
    gid: GuildId,
) -> Result<Vec<User>, CmdError> {
    use crate::schema::user::dsl::{discord_id as discord_id_dsl, user as user_dsl};

    let mut members = MembersIter::<Http>::stream(ctx, gid).boxed();
    let mut ids: Vec<u64> = Vec::new();
    let mut last_error = None;

    while let Some(member_result) = members.next().await {
        match member_result {
            Ok(member) => roles.iter().for_each(|role| {
                if member.roles.contains(role) {
                    ids.push(member.user.id.0);
                }
            }),
            Err(error) => {
                error!("Uh oh!  Error: {}", error);
                last_error = Some(error)
            }
        }
    }

    if let Some(e) = last_error {
        return Err(e.into());
    }

    ids.sort_unstable();
    ids.dedup();

    let conn = get_connection();

    Ok(user_dsl
        .filter(discord_id_dsl.eq_any(ids))
        .load::<User>(&conn)?)
}

pub fn get_user_from_discord_user(d_user: &DiscordUser) -> Result<User, diesel::result::Error> {
    use crate::schema::user::dsl::*;
    let conn = get_connection();

    user.filter(discord_id.eq(d_user.id.0)).first(&conn)
}

pub fn stars_to_shards(stars: u16) -> u16 {
    match stars {
        0 => 0,
        1 => 10,
        2 => 25,
        3 => 50,
        4 => 80,
        5 => 145,
        6 => 230,
        _ => 330,
    }
}

#[allow(unused_macros)]
#[macro_use]
pub mod macros {
    macro_rules! catch_error {
        ($e:expr) => {
            $e.map_err(|e| {
                error!("error: {}", e.to_string());
                CommandError(e.to_string)
            })
        };
    }
}
