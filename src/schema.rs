table! {
    farm_entry (id) {
        id -> Integer,
        user_farm_id -> Integer,
        comments -> Nullable<Varchar>,
        current_shards -> Unsigned<Smallint>,
        current_stars -> Unsigned<Smallint>,
        date -> Datetime,
    }
}

table! {
    unit (id) {
        id -> Integer,
        name -> Varchar,
        base_id -> Varchar,
        activate_shard_count -> Integer,
    }
}

table! {
    user (id) {
        id -> Integer,
        discord_id -> Unsigned<Bigint>,
        discord_username -> Varchar,
        swgoh_ally_code -> Unsigned<Bigint>,
        swgoh_username -> Varchar,
        ignored -> Bool,
    }
}

table! {
    user_farm (id) {
        id -> Integer,
        unit_id -> Integer,
        user_id -> Integer,
        gears_goal -> Nullable<Unsigned<Smallint>>,
        gears_finished -> Bool,
        stars_goal -> Nullable<Unsigned<Smallint>>,
        stars_finished -> Bool,
    }
}

joinable!(farm_entry -> user_farm (user_farm_id));
joinable!(user_farm -> unit (unit_id));
joinable!(user_farm -> user (user_id));

allow_tables_to_appear_in_same_query!(farm_entry, unit, user, user_farm,);
