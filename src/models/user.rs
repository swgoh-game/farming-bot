use crate::schema::user;
use diesel::dsl::Select;
use diesel::prelude::*;

type AllColumns = (
    user::id,
    user::discord_id,
    user::discord_username,
    user::swgoh_ally_code,
    user::swgoh_username,
);

pub const ALL_COLUMNS: AllColumns = (
    user::id,
    user::discord_id,
    user::discord_username,
    user::swgoh_ally_code,
    user::swgoh_username,
);

type All = Select<user::table, AllColumns>;

#[derive(Identifiable, Queryable, PartialEq, Debug, Clone)]
#[table_name = "user"]
pub struct User {
    pub id: i32,
    pub discord_id: u64,
    pub discord_username: String,
    pub swgoh_ally_code: u64,
    pub swgoh_username: String,
    pub ignored: bool,
}

impl User {
    pub fn all() -> All {
        user::table.select(ALL_COLUMNS)
    }
}

#[derive(Insertable)]
#[table_name = "user"]
pub struct NewUser<'a> {
    pub discord_id: &'a u64,
    pub discord_username: &'a String,
    pub swgoh_ally_code: &'a u64,
    pub swgoh_username: &'a String,
}
