use crate::schema::unit;
use api_swgoh_gg::model::{Ship as APIShip, Unit as APIUnit};

#[derive(Identifiable, Queryable, PartialEq, AsChangeset, Debug, Clone)]
#[table_name = "unit"]
pub struct Unit {
    pub id: i32,
    pub name: String,
    pub base_id: String,
    pub activate_shard_count: i32,
}

impl ToString for Unit {
    fn to_string(&self) -> String {
        self.name.clone()
    }
}

#[derive(Debug, Insertable)]
#[table_name = "unit"]
pub struct NewUnit {
    pub name: String,
    pub base_id: String,
    pub activate_shard_count: i32,
}

impl From<APIUnit> for NewUnit {
    fn from(u: APIUnit) -> Self {
        NewUnit {
            name: u.name,
            base_id: u.base_id,
            activate_shard_count: u.activate_shard_count as i32,
        }
    }
}

impl From<APIShip> for NewUnit {
    fn from(s: APIShip) -> Self {
        NewUnit {
            name: s.name,
            base_id: s.base_id,
            activate_shard_count: s.activate_shard_count as i32,
        }
    }
}
