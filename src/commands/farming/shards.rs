use crate::models::farm_entry::NewFarmEntry;
use chrono::prelude::Utc;

use super::*;
use crate::commands::unit::find_unit;

#[command]
#[aliases("sh")]
#[description = "Add a farm entry"]
#[usage = "unit shards [stars goals] [comment]"]
#[example = "vader 42/100 7 For solo rancor"]
pub async fn shards(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    use crate::schema::farm_entry::dsl::farm_entry;

    let conn = get_connection();

    let message = format!("user {} add {}", msg.author.id, args.rest());
    debug!("{}", message);

    let params: AddParams;
    match AddParams::parse(args, FarmType::Shard) {
        Ok(p) => params = p,
        Err(e) => {
            error!("{}", e);

            return if "end of string" == e.message {
                Err("Missing some args".into())
            } else {
                Err(e.into())
            };
        }
    }
    debug!("{:?}", params);

    let t = find_unit(&params.unit_name, &conn)?;
    let cu = get_current_user(msg.author.id.0)?;

    let mut farmed_unit = match get_farmed_unit(&cu, &t) {
        Some(ft) => ft,
        None => insert_farmed_unit(&cu, &t, &params, &conn, FarmType::Shard)?,
    };

    if params.goal_stars.is_some() && farmed_unit.stars_goal.unwrap() != params.goal_stars.unwrap()
    {
        farmed_unit.stars_goal = params.goal_stars;

        farmed_unit
            .save_changes::<UserFarm>(&*conn)
            .log_err(Some("Error during save"))?;
    }

    debug!("{:?}", farmed_unit);

    let new_farming = NewFarmEntry {
        comments: params.comments.as_ref(),
        current_shards: &params.current_shards,
        current_stars: &params.current_stars,
        date: &Utc::now().naive_utc(),
        user_farm_id: &farmed_unit.id,
    };

    let insert = diesel::insert_into(farm_entry)
        .values(&new_farming)
        .execute(&conn);
    match insert {
        Err(e) => Err(handle_diesel_error(e, &t).into()),
        Ok(_) => {
            if let Err(e) = msg
                .channel_id
                .say(ctx, farmed_unit.print_current_summary())
                .await
            {
                error!("[E021] Error during send farming shards reply: {}", e);
            }

            if let Some(e) = farmed_unit.get_last_entry() {
                farmed_unit.stars_finished =
                    0 == e.get_remaining_shards(farmed_unit.stars_goal.unwrap_or(7));

                farmed_unit
                    .save_changes::<UserFarm>(&*conn)
                    .log_err(Some("Error during save"))?;
            }

            Ok(())
        }
    }
}
