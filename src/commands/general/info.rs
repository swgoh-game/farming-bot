use diesel::sql_query;
use diesel::sql_types::Text;

use crate::internal::get_connection;

use super::*;

#[command]
#[description = "Print application info"]
#[allowed_roles("Officiers Rishi", "Officiers Ukio")]
pub async fn info(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    let conn = get_connection();

    let query: Vec<Info> = sql_query("SELECT VERSION() as version;").load(&conn)?;
    let app_version = option_env!("CARGO_PKG_VERSION")
        .unwrap_or("Unknown")
        .to_string();

    let fields = vec![
        ("MySQL version", &query[0].version, true),
        ("Application version", &app_version, true),
    ];
    msg.channel_id
        .send_message(&ctx.http, |m| m.embed(|e| e.fields(fields)))
        .await?;

    Ok(())
}

#[derive(QueryableByName, PartialEq, Debug)]
struct Info {
    #[sql_type = "Text"]
    version: String,
}
