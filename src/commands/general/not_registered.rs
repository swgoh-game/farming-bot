use crate::internal::get_connection;
use crate::schema::user::dsl::{discord_id as discord_id_dsl, user as user_dsl};
use diesel::prelude::*;
use serenity::model::id::GuildId;
use serenity::{
    framework::standard::{macros::command, Args, CommandResult},
    model::{guild::Member, prelude::Message, user::User as DiscordUser},
    prelude::Context,
};

#[command]
#[aliases("nr")]
#[description = "List all not registered users from mentions"]
#[usage = "tag"]
#[example = "@members"]
pub async fn not_registered(ctx: &Context, msg: &Message, mut _args: Args) -> CommandResult {
    let conn = get_connection();

    let gid = msg.guild_id.unwrap();

    let users: Vec<u64> = user_dsl.select(discord_id_dsl).load::<u64>(&conn)?;

    let mut not_registered_users: Vec<String> = Vec::new();

    for mention in &msg.mentions {
        add_users(ctx, gid, &users, mention, &mut not_registered_users).await;
    }

    // TODO change and use MemberStream instead and build an embed.
    let members: Vec<Member> = gid.members(&ctx, Some(1000), None).await?;
    for role in &msg.mention_roles {
        let has_role: Vec<DiscordUser> = members
            .iter()
            .filter(|m| m.roles.contains(role))
            .map(|m| m.user.clone())
            .collect();

        debug!("role: {}, users: {:?}", role, has_role);

        for user in &has_role {
            add_users(ctx, gid, &users, user, &mut not_registered_users).await
        }
    }

    let mut reply = "".to_string();
    for unregistered_user in not_registered_users {
        reply.push_str(&format!("{}\n", unregistered_user));
    }

    msg.channel_id
        .say(
            &ctx,
            format!(
                "```asciidoc\nUser not registered:\n===================\n{}```",
                reply
            ),
        )
        .await?;

    Ok(())
}

async fn add_users(
    ctx: &Context,
    gid: GuildId,
    users: &[u64],
    user: &DiscordUser,
    not_registered: &mut Vec<String>,
) {
    if !users.contains(&user.id.0) {
        not_registered.push(
            user.nick_in(&ctx, gid)
                .await
                .unwrap_or_else(|| user.name.clone()),
        );
    }
}
