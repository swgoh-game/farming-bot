use crate::get_connection;
use crate::internal::get_user_from_discord_user;
use crate::models::User;
use diesel::prelude::*;
use serenity::framework::standard::{macros::group, CommandResult};
use serenity::{
    framework::standard::{macros::command, Args},
    model::channel::Message,
    prelude::*,
    utils::MessageBuilder,
};

pub use self::ignore::*;
pub use self::info::*;
pub use self::not_registered::*;
pub use self::register::*;
pub use self::un_ignore::*;

mod ignore;
mod info;
mod not_registered;
mod register;
mod un_ignore;

#[group]
#[description = "Group for general commands"]
#[commands(info, ignore, register, not_registered, un_ignore)]
struct General;

fn update_ignore(msg: &Message, ignore_value: bool) -> CommandResult {
    use crate::schema::user::dsl::ignored;

    let conn = get_connection();

    let users: Vec<User> = msg
        .mentions
        .iter()
        .filter_map(|discord_user| get_user_from_discord_user(discord_user).ok())
        .collect();

    for user in users {
        diesel::update(&user)
            .set(ignored.eq(ignore_value))
            .execute(&conn)?;
    }

    Ok(())
}
