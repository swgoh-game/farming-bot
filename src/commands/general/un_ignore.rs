use super::*;

#[command]
#[aliases("uig")]
#[description = "Un ignore user"]
#[allowed_roles("Officiers Rishi", "Officiers Ukio")]
#[usage = "<USER>"]
#[example = "@foo"]
pub async fn un_ignore(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    update_ignore(msg, false)?;

    msg.reply(ctx, "User successfully un ignored.").await?;

    Ok(())
}
