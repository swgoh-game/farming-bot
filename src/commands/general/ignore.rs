use super::*;

#[command]
#[aliases("ig")]
#[description = "Ignore user"]
#[allowed_roles("Officiers Rishi", "Officiers Ukio")]
#[usage = "<USER>"]
#[example = "@foo"]
pub async fn ignore(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    update_ignore(msg, true)?;

    msg.reply(ctx, "User successfully ignored.").await?;

    Ok(())
}
