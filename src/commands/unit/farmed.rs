use super::*;
use crate::models::{User, UserFarm};
use crate::schema::unit::dsl::unit as dsl_unit;
use crate::schema::user::dsl::user;

#[command]
#[aliases("fmd")]
#[description = "List user who farm a unit"]
#[usage = "<UNIT>"]
#[example = "thrawn"]
async fn farmed(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    // TODO Refactor this command to accept a list of unit.
    //  if no unit, check the limitations of discord to see if it is possible to send every units.
    let unit_name = args.rest();
    let conn = get_connection();

    let units = if unit_name.is_empty() {
        dsl_unit
            .select(dsl_unit::all_columns())
            .load::<Unit>(&conn)?
    } else {
        vec![find_unit(unit_name, &conn)?]
    };

    for unit in units {
        unit_farmed(ctx, msg, unit).await?;
    }

    Ok(())
}

async fn unit_farmed(ctx: &Context, msg: &Message, unit: Unit) -> CommandResult {
    let conn = get_connection();

    let all_users: Vec<User> = user.load(&conn)?;

    let farms: Vec<UserFarm> = UserFarm::belonging_to(&unit).load(&conn)?;
    if farms.is_empty() {
        return Ok(());
    }

    let users: Vec<String> = farms
        .iter()
        .map(|f| {
            all_users
                .iter()
                .find(|u| u.id == f.user_id)
                .map_or("Unknown".to_string(), |u| u.discord_username.clone())
        })
        .collect();
    let remaining_times: Vec<String> = farms
        .iter()
        .map(|f| {
            f.get_remaining_days()
                .map_or("Unknown".to_string(), |days| days.to_string())
        })
        .collect();

    let users_field = if users.is_empty() {
        "No user".to_string()
    } else {
        users.join("\n")
    };
    let remaining_field = if remaining_times.is_empty() {
        "No user".to_string()
    } else {
        remaining_times.join("\n")
    };
    debug!("fields: {}, remaining: {}", users_field, remaining_field);

    msg.channel_id
        .send_message(&ctx.http, |m| {
            m.embed(|e| {
                e.title(format!("Users who farm {}", unit.to_string()))
                    .field("User", users_field, true)
                    .field("Remaining days", remaining_field, true)
            })
        })
        .await?;

    Ok(())
}
