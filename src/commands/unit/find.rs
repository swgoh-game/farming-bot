use super::*;
use crate::schema::unit::dsl::*;

#[command]
#[aliases("fnd")]
#[description = "Find unit registered in bot"]
#[usage = "unit"]
#[example = "darth"]
async fn find(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let unit_name = args.rest();

    let conn = get_connection();

    let units: Vec<String> = unit
        .select(name)
        .filter(name.like(format!("%{}%", unit_name.replace(" ", "%"))))
        .order(name.asc())
        .load(&conn)?;

    msg.channel_id
        .send_message(&ctx.http, |m| {
            m.embed(|e| e.field("Units", units.join("\n"), true))
        })
        .await?;

    Ok(())
}
