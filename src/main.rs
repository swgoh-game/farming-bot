extern crate api_swgoh_gg;
extern crate chrono;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;
extern crate dotenv;
extern crate env_logger;
#[macro_use]
extern crate log;
extern crate numerals;
extern crate serenity;
extern crate time;
#[macro_use]
extern crate lazy_static;
extern crate r2d2;

use std::collections::HashSet;
use std::env;

use dotenv::dotenv;
use serenity::http::Http;
use serenity::{
    async_trait,
    framework::standard::{
        help_commands,
        macros::{help, hook},
        Args, CommandGroup, CommandResult, HelpOptions, StandardFramework,
    },
    model::{
        event::ResumedEvent,
        gateway::Ready,
        prelude::{Message, UserId},
    },
    prelude::*,
    utils::MessageBuilder,
};

use commands::farming::*;
use commands::general::*;
use commands::unit::*;
use internal::get_connection;

mod commands;
mod internal;
mod models;
mod schema;

embed_migrations!();

#[help]
async fn my_help(
    context: &Context,
    msg: &Message,
    args: Args,
    help_options: &'static HelpOptions,
    groups: &[&'static CommandGroup],
    owners: HashSet<UserId>,
) -> CommandResult {
    help_commands::with_embeds(context, msg, args, help_options, groups, owners).await;
    Ok(())
}

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, _: Context, ready: Ready) {
        info!("Connected as {}", ready.user.name);
    }

    async fn resume(&self, _: Context, _: ResumedEvent) {
        info!("Resumed");
    }
}

#[hook]
async fn before(ctx: &Context, msg: &Message, command_name: &str) -> bool {
    if let Err(e) = msg.react(ctx, '✋').await {
        error!("[E001] Error during reaction in before cmd: {}", e);
    };

    info!(
        "Got command '{}' by user '{}'",
        command_name, msg.author.name
    );

    true // if `before` returns false, command processing doesn't happen.
}

#[hook]
async fn after(ctx: &Context, msg: &Message, command_name: &str, command_result: CommandResult) {
    match command_result {
        Ok(()) => {
            if let Err(e) = msg.react(ctx, '👌').await {
                error!("[E002] Error during reaction in after cmd: {}", e);
            };

            info!("Processed command '{}'", command_name)
        }
        Err(why) => {
            if let Err(e) = msg.react(ctx, '☠').await {
                error!("[E003] Error during reaction in after cmd: {}", e);
            };

            error!("Command '{}' returned error {:?}", command_name, why);

            if let Err(e) = msg
                .channel_id
                .say(
                    ctx,
                    MessageBuilder::new()
                        .mention(&msg.author)
                        .push(format!(" {}", why))
                        .build(),
                )
                .await
            {
                error!("[E004] Error during send of error: {}", e);
            }
        }
    };
}

#[hook]
async fn unknown_command(ctx: &Context, msg: &Message, unknown_command_name: &str) {
    let txt = format!("Unknown command: {}", unknown_command_name);
    warn!("{}", txt);
    if let Err(e) = msg.channel_id.say(ctx, txt).await {
        error!("{}", e)
    }
}

#[tokio::main]
async fn main() {
    // This will load the environment variables located at `./.env`, relative to
    // the CWD. See `./.env` for an example on how to structure this.
    dotenv().ok();

    // Initialize the logger to use environment variables.
    //
    // In this case, a good default is setting the environment variable
    // `RUST_LOG` to debug`.
    env_logger::init();

    // Initialize diesel
    let conn = get_connection();

    embedded_migrations::run_with_output(&conn, &mut std::io::stdout()).unwrap();

    let token = env::var("DISCORD_TOKEN").expect("Expected a token in the environment");
    let prefix = env::var("DISCORD_PREFIX").unwrap_or("!".to_string());
    debug!("Discord prefix: {}", prefix);

    let http = Http::new_with_token(&token);

    // We will fetch your bot's owners and id
    let (owners, bot_id) = match http.get_current_application_info().await {
        Ok(info) => {
            let mut owners = HashSet::new();
            if let Some(team) = info.team {
                owners.insert(team.owner_user_id);
            } else {
                owners.insert(info.owner.id);
            }
            match http.get_current_user().await {
                Ok(bot_id) => (owners, bot_id.id),
                Err(why) => panic!("Could not access the bot id: {:?}", why),
            }
        }
        Err(why) => panic!("Could not access application info: {:?}", why),
    };

    let framework = StandardFramework::new()
        .configure(|c| {
            c.with_whitespace(true)
                .on_mention(Some(bot_id))
                .prefix(prefix.as_str())
                .owners(owners)
        })
        .unrecognised_command(unknown_command)
        .before(before)
        .after(after)
        .help(&MY_HELP)
        .group(&GENERAL_GROUP)
        .group(&UNITS_GROUP)
        .group(&FARM_GROUP);

    let mut client = Client::builder(&token)
        .event_handler(Handler)
        .framework(framework)
        .await
        .expect("Err creating client");

    if let Err(why) = client.start().await {
        error!("Client error: {:?}", why);
    }
}
