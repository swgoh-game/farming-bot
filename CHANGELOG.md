# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [unreleased]

## [0.5.0] - 2022-02-15

### Changed

- Update dependencies. #12
- Update to serenity 0.10. #13
- Allow unit farmed command to be executed without any unit name. #10

## [0.4.3] - 2021-11-23

### Changed

- Add more explicit message for ticket #8
- Modify establish connection function
- Remove deprecated

## [0.4.2] - 2020-05-24

### Fixed

- Fix username with non ascii code

## [0.4.1] - 2020-02-23

### Fixed

- Fix selection of database from app env. (David Wittwer)

## [0.4.0] - 2020-02-23

### Changed

- Update serenity to 0.8 and update other dependencies. (David Wittwer)

### Fixed

- Remaining time computing. (David Wittwer)

## [0.3.0] - 2019-12-06

### Added

- Add possibility to ignore member. !7 (David Wittwer)
- Run migrations at application startup. !8 (David Wittwer)
- Command to list users who farm a unit. !5 (David Wittwer)
- Command to update units from swgoh.gg API. !3 (David Wittwer)

### Fixed

- Fix remaining days estimation. (David Wittwer)

## [0.2.0] 2019-08-22

### Changed

- Update to Serenity 0.6. !1 (David Wittwer)

## [0.1.0] 2019-08-21

## Added

- register command
- not registered command
- unit list command
- unit find command
- farm shards command
